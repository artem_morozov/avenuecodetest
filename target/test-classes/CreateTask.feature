@CreateTask
Feature: Create Task
  #As a To Do App user I should be able to create a task So I can manage my tasks
  @CreateTask
  Scenario Outline: User can sign in and create a task
    Given User opens the Avenue website "<website>"
    When User Login with "<email>" and "<password>"
    Then User clicks on MyTasks link, which belongs to "<username>"
    When User verifies MyTasks list was updated after adding a task "test"
    Then User Logout
    Examples:
      | website                         | email                | password   | username      |
      | https://qa-test.avenuecode.com/ | morozov383@gmail.com | avenueTest | Artem Morozov |