package stepdefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.testng.Reporter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Hooks {
    public static WebDriver driver;



    @Before
    /**
     * Delete all cookies at the start of each scenario to avoid
     * shared state between tests
     */
    public void openBrowser() {
        System.out.println("\nCalled openBrowser:");

        driver = new ChromeDriver();
        //driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(0, 0)); //window will be always in left top corner
        //driver.manage().window().setSize(new Dimension(1234, 750)); //For MacBook Pro 13' for fullscreen
    }

    /** Closing browser after each run */
    @After
    public void closeBrowser() {
        driver.close();
    }
    /** Embed a screenshot in test report if test is marked as failed */
    /*public void embedScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            String destDir;
            DateFormat dateFormat;
            // Set folder name to store screenshots.
            destDir = "screenshots";
            // Capture screenshot.
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            // Set date format to set It as screenshot file name.
            dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
            // Create folder under project with name "screenshots" provided to destDir.
            new File(destDir).mkdirs();
            // Set file name using current date time.
            String destFile = dateFormat.format(new Date()) + ".png";

            try {
                // Copy paste file at destination folder location
                FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            Reporter.log("Scenario is passed");
        driver.quit();
    }*/
} // end of stepdefinitions.Hooks