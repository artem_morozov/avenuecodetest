package stepdefinitions;

import PageObjects.HomePage;
import PageObjects.MyTasksPage;
import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class CreateSubTask {
    private static WebDriver driver;

    public CreateSubTask() {
        driver = Hooks.driver;
    }

    @When("^User is able to see sub task manage button$")
    public void user_can_see_subTask_manage_button() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        PageFactory.initElements(driver, MyTasksPage.class);
        HomePage.myTasksButton.click();
        if(MyTasksPage.firstSubTaskManageButton.isDisplayed()) {
            System.out.println("First sub task manage button is displayed \n");
        }
    }

    private String subTaskNumber = "";
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private String date = dateFormat.format(new Date());

    @When("^User clicks on sub task manage button and creates a sub task$")
    public void user_clicks_on_subTask_manage_button() {
        PageFactory.initElements(driver, MyTasksPage.class);
        if(MyTasksPage.firstSubTaskManageButton.isEnabled())
            subTaskNumber = MyTasksPage.firstSubTaskManageButton.getText();
        //getting an amount of sub tasks in int
        int num = Integer.parseInt(subTaskNumber.replaceAll("[\\D]", ""));
        MyTasksPage.firstSubTaskManageButton.click();

        Random random = new Random();
        int subTaskNum = random.nextInt(1000);
        String subTaskText = "sub task" + subTaskNumber;
        MyTasksPage.newSubTaskTextBox.sendKeys(subTaskText); //  + Keys.RETURN
        MyTasksPage.addSubTask.click();

        if(MyTasksPage.dueDateSubTask.isEnabled()) {
            String currentDate = MyTasksPage.dueDateSubTask.getText();
            //deleting current date since it's not correct
            for(int i = 0; i < currentDate.length(); i++) {
                MyTasksPage.dueDateSubTask.sendKeys(Keys.BACK_SPACE);
            }
            MyTasksPage.dueDateSubTask.sendKeys(date);
        }

        if(MyTasksPage.addedSubTaskAtTheTop.isDisplayed() &&
                MyTasksPage.addedSubTaskAtTheTop.getText().equals(subTaskText)) {
            System.out.println("Added task is displayed and correct");
        }

        MyTasksPage.closeButton.click();

        //checking if number of sub tasks is correct after adding one
        if(MyTasksPage.firstSubTaskManageButton.isEnabled()) {
            String compareNumber = MyTasksPage.firstSubTaskManageButton.getText();
            int compare = Integer.parseInt(compareNumber.replaceAll("[\\D]", ""));
            if(compare > num && (compare - num) == 1) {
                System.out.println("Sub task added correctly \n\n");
            }
        }
    }
} // end of CreateSubTask