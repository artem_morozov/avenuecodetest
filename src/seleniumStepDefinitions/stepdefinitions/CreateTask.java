package stepdefinitions;

import PageObjects.HomePage;
import PageObjects.MyTasksPage;
import PageObjects.NavBar;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

public class CreateTask {
    private static WebDriver driver;
    //Actions actions = new Actions(driver); //for using keyboard

    public CreateTask() {
        driver = Hooks.driver; // init driver from stepdefinitions.Hooks class
    }

    /** opens the homepage by URL */
    @Given("^User opens the Avenue website \"([^\"]*)\"$")
    public void user_opens_the_site(String url) {
        driver.get(url);
    }

    @When("^User Login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void user_login_with_username_and_password(String email, String password) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        PageFactory.initElements(driver, HomePage.class);
        PageFactory.initElements(driver, NavBar.class);
        NavBar.signIn.click();
        AssertJUnit.assertEquals("https://qa-test.avenuecode.com/users/sign_in",
                driver.getCurrentUrl()); //assertion for correct sign in url
        HomePage.emailInput.sendKeys(email);
        HomePage.passwordInput.sendKeys(password);
        HomePage.signInButton.click();
        if(HomePage.signInMessage.isDisplayed()) System.out.println("Signed in successfully \n");
        if(NavBar.myTasksLink.isDisplayed()) System.out.println("MyTasks link on the NavBar is displayed \n");
    }

    @When("^User clicks on MyTasks link, which belongs to \"([^\"]*)\"$")
    public void user_clicks_on_myTasks_link(String username) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        PageFactory.initElements(driver, MyTasksPage.class);
        PageFactory.initElements(driver, NavBar.class);
        NavBar.myTasksLink.click();
        WebElement name = driver.findElement(By.xpath("//h1[contains(text(), '" + username + "')]"));
        if(name.isDisplayed()) System.out.println(username + "'s ToDo list is displayed \n");
    }

    @When("^User verifies MyTasks list was updated after adding a task \"([^\"]*)\"$")
    public void user_verifies_myTasks_list(String testName) {
        PageFactory.initElements(driver, MyTasksPage.class);
        MyTasksPage.newTaskTextBox.sendKeys(testName + Keys.RETURN);
        WebElement firstTaskText = driver.findElement(By.xpath("(//tbody/tr[@class='ng-scope']/td[2]/a)[1]"));
        if(MyTasksPage.firstTaskFromTheList.isDisplayed() &&
                testName.equals(firstTaskText.getText())) {
            System.out.println("Task added correctly \n");
        } else {
            System.out.println("Task added incorrectly \n");
        }
    }

    @Then("^User Logout$")
    public void user_Logout_successfully() {
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        PageFactory.initElements(driver, HomePage.class);
        PageFactory.initElements(driver, NavBar.class);
        NavBar.signOut.click();
        AssertJUnit.assertEquals("https://qa-test.avenuecode.com/",
                driver.getCurrentUrl()); //assertion for correct sign out url
        if(HomePage.signOutMessage.isDisplayed()) {
            System.out.println("Signed out successfully \n");
            System.out.println("=======================");
        }
    }
} // end of CreateTask