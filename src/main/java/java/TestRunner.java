package java;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Basically this class is a runner for cucumber features which also is creating reports after each run.
 * I did not make it work on my machine, unfortunately I didn't have that much time to troubleshoot my Mac.
 * To run test cases you need to just run the Cucumber feature file and it will work.
 * I used Chrome since it is the most popular browser. Chrome driver is in AvenueCodeTest folder.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"AvenueCodeTest"},
        format = {"pretty", "html:reports/test-report", "json:target/cucumber-html-report/cucumber.json"},
        //glue = {"stepdefinitions"},
        tags = {"@One"}
)

public class TestRunner {

}
// end of java.TestRunner