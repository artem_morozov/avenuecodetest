package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavBar extends BaseClassPage {

    public NavBar(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Sign In") // /users/sign_in
    public static WebElement signIn;

    @FindBy(linkText = "Register") // /users/sign_up
    public static WebElement register;

    @FindBy(xpath = "//li/a[@href = '/tasks']")
    public static WebElement myTasksLink;

    @FindBy(className = "navbar-brand")
    public static WebElement ToDoAppLink;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']/li/a[@href='/']")
    public static WebElement homeLink;

    // that link doesn't redirect anywhere
    @FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right']/li/a[@href='#']")
    public static WebElement userLink;

    @FindBy(linkText = "Sign out") // /users/sign_out
    public static WebElement signOut;

} // end of PageObjects.NavBar