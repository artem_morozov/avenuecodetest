package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Footer extends BaseClassPage {
    public Footer(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Avenue Code") // http://www.avenuecode.com.br
    public static WebElement avenueCodeLink;

    @FindBy(linkText = "Test Description - User Stories") // user-stories
    public static WebElement userStoriesLink;

    @FindBy(linkText = "Track a Bug") // /bugs
    public static WebElement trackBugLink;
} // end of PageObjects.Footer