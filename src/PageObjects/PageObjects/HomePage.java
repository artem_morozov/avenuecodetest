package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BaseClassPage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "My Tasks")
    public static WebElement myTasksButton;

    @FindBy(className = "btn btn-lg btn-success") // /users/sign_up
    public static WebElement signUpButton;

    @FindBy(className = "btn btn-lg btn-danger") // user-stories
    public static WebElement testDescriptionButton;

    @FindBy(linkText = "Sign up") // /users/sign_up
    public static WebElement signUpLink; // after redirecting to 'sign in' page

    @FindBy(xpath = "//input[@id='user_email']")
    public static WebElement emailInput;

    @FindBy(xpath = "//input[@id='user_password']")
    public static WebElement passwordInput;

    @FindBy(id = "user_remember_me")
    public static WebElement rememberMeCheckBox;

    @FindBy(xpath = "//input[@class='btn btn-primary']")
    public static WebElement signInButton;

    //messages on a page
    @FindBy(xpath = "//div[contains(text(), 'Signed in successfully.')]")
    public static WebElement signInMessage;

    @FindBy(xpath = "//div[contains(text(), 'Signed out')]")
    public static WebElement signOutMessage;
} // end of PageObjects.HomePage