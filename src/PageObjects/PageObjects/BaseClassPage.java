package PageObjects;

import org.openqa.selenium.WebDriver;

public class BaseClassPage {
    public static WebDriver driver;

    public  BaseClassPage(WebDriver driver){  //constructor must be driver from static var
        BaseClassPage.driver = driver;

    }

} // end of PageObjects.BaseClassPage