package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyTasksPage extends BaseClassPage {
    public MyTasksPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "new_task")
    public static WebElement newTaskTextBox;

    @FindBy(className = "input-group-addon glyphicon glyphicon-plus")
    public static WebElement addNewTaskButton;

    @FindBy(xpath = "(//tbody/tr[@class='ng-scope'])[1]")
    public static WebElement firstTaskFromTheList;

    @FindBy(xpath = "(//tbody/tr[@class='ng-scope']/td[4]/button)[1]")
    public static WebElement firstSubTaskManageButton;

    //sub task modal frame
    @FindBy(id = "edit_task")
    public static WebElement editTaskTextBox;

    @FindBy(id = "new_sub_task")
    public static WebElement newSubTaskTextBox;

    @FindBy(id = "dueDate")
    public static WebElement dueDateSubTask;

    @FindBy(id = "add-subtask")
    public static WebElement addSubTask;

    @FindBy(xpath = "//div[@class='modal-footer ng-scope']/button")
    public static WebElement closeButton;

    @FindBy(xpath = "(//tr/td[@class='task_body col-md-8']/a)[1]")
    public static WebElement addedSubTaskAtTheTop;
} // end of MyTasksPage